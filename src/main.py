"""ILI9341 demo (bouncing boxes)."""
from machine import Pin, SPI
from random import random, seed
from ili9341 import Display, color565
from fontloader import XglcdFont
from xpt2046 import Touch
from utime import sleep_us, ticks_cpu, ticks_us, ticks_diff, sleep, time

print('Loading fonts...')
print('Loading arcadepix')
arcadepix = XglcdFont('fonts/ArcadePix9x11.c', 9, 11)
FONT_WIDTH = 9
FONT_HEIGHT = 11

class Box(object):
    """Bouncing box."""

    def __init__(self, screen_width, screen_height, size, display, color):
        """Initialize box.
        Args:
            screen_width (int): Width of screen.
            screen_height (int): Width of height.
            size (int): Square side length.
            display (ILI9341): display object.
            color (int): RGB565 color value.
        """
        self.size = size
        self.w = screen_width
        self.h = screen_height
        self.display = display
        self.color = color
        # Generate non-zero random speeds between -5.0 and 5.0
        seed(ticks_cpu())
        r = random() * 10.0
        self.x_speed = 5.0 - r if r < 5.0 else r - 10.0
        r = random() * 10.0
        self.y_speed = 5.0 - r if r < 5.0 else r - 10.0

        self.x = self.w / 2.0
        self.y = self.h / 2.0
        self.prev_x = self.x
        self.prev_y = self.y

    def update_pos(self):
        """Update box position and speed."""
        x = self.x
        y = self.y
        size = self.size
        w = self.w
        h = self.h
        x_speed = abs(self.x_speed)
        y_speed = abs(self.y_speed)
        self.prev_x = x
        self.prev_y = y

        if x + size >= w - x_speed:
            self.x_speed = -x_speed
        elif x - size <= x_speed + 1:
            self.x_speed = x_speed

        if y + size >= h - y_speed:
            self.y_speed = -y_speed
        elif y - size <= y_speed + 1:
            self.y_speed = y_speed

        self.x = x + self.x_speed
        self.y = y + self.y_speed

    def draw(self):
        """Draw box."""
        x = int(self.x)
        y = int(self.y)
        size = self.size
        prev_x = int(self.prev_x)
        prev_y = int(self.prev_y)
        self.display.fill_hrect(prev_x - size,
                                prev_y - size,
                                size, size, 0)
        self.display.fill_hrect(x - size,
                                y - size,
                                size, size, self.color)

    def cleanup(self, color=color565(255, 255, 255)):
        """Draw box."""
        x = int(self.x)
        y = int(self.y)
        size = self.size
        prev_x = int(self.prev_x)
        prev_y = int(self.prev_y)
        self.display.fill_hrect(prev_x - size,
                                prev_y - size,
                                size, size, color)
        self.display.fill_hrect(x - size,
                                y - size,
                                size, size, color)

class UI:
    def  __init__(self, display, spi_touch):
        self.display = display
        self.touch = Touch(spi_touch, cs=Pin(5), int_pin=Pin(0), int_handler=self.touchscreen_interrupt)

        self.screenSaver = True
        self.screenSaverWait = 15
        self.lastUserInput = 0

        self.colors = [color565(255, 0, 0),
                  color565(0, 255, 0),
                  color565(0, 0, 255),
                  color565(255, 255, 0),
                  color565(0, 255, 255),
                  color565(255, 0, 255)]
        self.sizes = [12, 11, 10, 9, 8, 7]
        self.boxes = [Box(239, 319, self.sizes[i], self.display,
                 self.colors[i]) for i in range(6)]

    def next_frame(self):
        if time() - self.lastUserInput >= self.screenSaverWait and self.screenSaver == False:
            self.screenSaver = True
            self.display.clear()

        if self.screenSaver == True:
            self.screensaver_frame();
        else:
            self.ui_frame();

    def touchscreen_interrupt(self, x, y):
        # Call function when the display has detected a touch
        print("The screen has detected a touch")
        self.lastUserInput = time()

        if self.screenSaver == True:
            self.screenSaver = False
            self.screensaver_cleanup()
            self.first_ui_frame()

    def screensaver_frame(self):
        for b in self.boxes:
            b.update_pos()
            b.draw()

    def screensaver_cleanup(self):
        for b in self.boxes:
            b.cleanup()

    def first_ui_frame(self):
        self.display.clear()
        self.display.fill_rectangle(0, 0, 240, 320, color565(255, 255, 255))

        text = [["l1", "l2", "l3", "l4"], ["r1", "r2", "r3", "r4"]]

        self.ui_grid(text)

        self.ui_frame()

    def ui_frame(self):
        pass

    def ui_grid(self, text, colums=2, rows=4, spacing = 10):

        rec_w = 260/colums-spacing*2
        rec_h = 360/rows-spacing*2

        for row in range(0, rows):
            for colum in range(0, colums):
                x_rec = int((spacing/2)+(colum*(rec_w+spacing)))
                y_rec = int((spacing/2)+(row*(rec_h+spacing)))
                y_t = int(y_rec+((rec_h-FONT_HEIGHT)/2)) 
                self.display.draw_text(x_rec, y_t, text[colum][row], arcadepix, color565(255, 0, 0))
                self.display.draw_rectangle(x_rec, y_rec,
                        int(rec_w), int(rec_h), int(color565(0, 0, 0)))



def main():
    ### SETUP DISPLAY ###
    # Baud rate of 40000000 seems about the max
    spi_dis = SPI(1, baudrate=40000000, sck=Pin(14), mosi=Pin(13))
    display = Display(spi_dis, dc=Pin(4), cs=Pin(16), rst=Pin(17))

    ### SETUP TOUCH SCREEN ###
    spi_touch = SPI(2, baudrate=1000000, sck=Pin(18), mosi=Pin(23), miso=Pin(19));

    try:
        display.clear()

        ui = UI(display, spi_touch)

        while True:
            ui.next_frame()
    
    except KeyboardInterrupt:
        print("\nCtrl-C pressed.  Cleaning up and exiting...")

    finally:
        print("error: the program has quitted")
        display.clear()
        display.draw_text(10, 10, "error: the program has quitted", arcadepix, color565(255, 0, 0))

main()
