#!/bin/bash

if [ -z "$1" ]
then
     echo "error: enter device path as first parameter"
     
else
  for file in ./src/*; 
  do
      echo "uploading $file ..."
      ampy -p $1 put $file
      echo "uploaded $file"; 
  done

fi
