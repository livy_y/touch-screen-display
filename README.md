# Touch Screen Display

This is a small project to controll a TFT SPI display with esp32 and micropython. The display shows some very basic UI and a screensaver. The screensaver are boxes that bounce around the screen.

![The screensaver](./images/screensaver.jpg)

## Installation 
To test the project for yourself you first need to upload the micropython firmware to the esp32 with [this documentation](https://docs.micropython.org/en/latest/esp32/tutorial/intro.html). Then you need to install [picocom](https://man.archlinux.org/man/picocom.1.en) and [ampy](https://github.com/scientifichackers/ampy) so that you can comunicate with the esp32. Finaly you can execute upload.sh (in this root directory), this will upload all the files to the esp32. You need to specify the usb port after the program for example `./upload.sh /dev/ttyUSB0`.
Now you need to connect the display to the esp32, for this look at the table below.
If you want to see the output of the esp32, you can use picocom like this `picocom /dev/ttyUSB0 -b115200`.

| esp32 pin    | display pin          |
|--------------|----------------------|
| 14           | sck                  |
| 13           | mosi                 |
| 4            | dc                   |
| 16           | cs                   |
| 17           | rst/reset            |
|              |                      |
| 18           | t\_sck      / t\_clk |
| 23           | t\_mosi     / t\_din |
| 19           | t\_miso     / t\_do  |
| 5            | t\_cs                |
| 0            | t\_int\_pin / t\_irq |

![Display pinout](./images/displayLowerSide.jpg)
![Side view](./images/sideView.jpg)
![Top down view](./images/topDownView.jpg)


## Usage
If you want to change the UI that is displayed you need to look into the methode called first\_ui\_frame. Or if you want to use touch data then you need to look into the methode called touchscreen\_interrupt, there the x and y parameter is where the user touched the display.

## License
All the files are licensed under the MIT license.

## Project status
The project is finished yet there is room for improvements like better ui and maybe bug fixes.
